package util;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import annotation.Annotation;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import model.MethodAndClass;
import exception.UrlNotSupportedException;

public class Utilitaire {
    
    public static String retrieveUrlFromRawurl(String url){
        String[] urlDecomposer = url.split("/");
        String urlIlaina ="";
        for(int i=0;i<urlDecomposer.length;i++){
            urlIlaina = urlDecomposer[i];
        }
        return urlIlaina;
    }
    
    public ArrayList<Method> detectAllAnnotation(Class c){

        Method[] fonctions = c.getDeclaredMethods();
        ArrayList<Method> annotation = new ArrayList();
        for(int i=0;i<fonctions.length;i++){
            if(fonctions[i].isAnnotationPresent(Annotation.class)){
                annotation.add(fonctions[i]);
            }
        }     
        return annotation;
    }
    
    /*------------------------------------------- Parcourir classe et méthode -----------------------------------------------------*/
    
    private static List<Class<?>> findClasses(File repertoire, String nomPackage) {
        List<Class<?>> classes = new ArrayList<>();
        if (!repertoire.exists()) {
            return classes;
        }
        File[] files = repertoire.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file,
                        (!nomPackage.equals("") ? nomPackage + "." : nomPackage) + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                try {
                    classes.add(Class
                            .forName(nomPackage + '.' + file.getName().substring(0, file.getName().length() - 6)));
                } catch (ClassNotFoundException e) {
                    System.err.println(e.getMessage());
                }
            }
        }
        return classes;
    }
    
    public static Class<?>[] getClasses(String nomPackage) {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        String path = nomPackage.replace('.', '/');
        Enumeration<URL> resources = null;
        try {
            resources = classLoader.getResources(path);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        List<File> dirs = new ArrayList<>();
        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            dirs.add(new File(resource.getFile()));
        }
        List<Class<?>> classes = new ArrayList<>();
        dirs.forEach((directory) -> {
            classes.addAll(findClasses(directory, nomPackage));
            
        });
        return classes.toArray(new Class[classes.size()]);
    }

    public static MethodAndClass urlExist(HashMap<String, MethodAndClass> myMap, String url) throws UrlNotSupportedException {
        
        String urlSearch = Utilitaire.retrieveUrlFromRawurl(url);
        for (HashMap.Entry suite : myMap.entrySet()) {
            if (urlSearch.equals((String)suite.getKey())) {
                return myMap.get(urlSearch);
            }
        }
        throw new UrlNotSupportedException("UrlNotSupportedException");
    } 
}
