package model;

public class MethodAndClass {
    private String classe;
    private String method;

    public MethodAndClass() {
    }

    public MethodAndClass(String classe, String method) {
        this.classe = classe;
        this.method = method;
    }

    public String getClasse() {
        return classe;
    }

    public String getMethod() {
        return method;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    public void setMethod(String method) {
        this.method = method;
    }
    
}
