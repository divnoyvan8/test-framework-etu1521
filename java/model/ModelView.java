package model;

import java.util.HashMap;

public class ModelView {
        HashMap data;
        String url;

        public HashMap getData() {
            return data;
        }

        public void setData(HashMap data) {
            this.data = data;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
}
