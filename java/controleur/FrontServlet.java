package controleur;

import exception.UrlNotSupportedException;
import util.Utilitaire;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.MethodAndClass;
import model.ModelView;

public class FrontServlet extends HttpServlet {

    public void init() throws ServletException {
        ServletContext context = this.getServletContext();
        Utilitaire fonction = new Utilitaire();

        HashMap<String, MethodAndClass> associationUrlAndMethod = new HashMap<>();
        Class<?>[] a = fonction.getClasses("");
        ArrayList<Method> listeMethodAnnotation = new ArrayList<>();
        MethodAndClass association = new MethodAndClass();
        for (int i = 0; i < a.length; i++) {
            if (fonction.detectAllAnnotation(a[i]).isEmpty()) {
                continue;
            }
            listeMethodAnnotation = fonction.detectAllAnnotation(a[i]);
            Method methodAnnotation = listeMethodAnnotation.get(i);
            Method[] methode = a[i].getMethods();
            for (int i2 = 0; i2 < methode.length; i2++) {
                association.setClasse(a[i].getName());
                association.setMethod(methode[i2].getName());
                associationUrlAndMethod.put(methodAnnotation.getName(), association);
            }

        }

        context.setAttribute("context", associationUrlAndMethod);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setContentType("text/plain");
        try {
            PrintWriter out = response.getWriter();
            ServletContext context = this.getServletContext();

            String url = Utilitaire.retrieveUrlFromRawurl(request.getRequestURI());

            HashMap<String, MethodAndClass> myMap = (HashMap<String, MethodAndClass>) context.getAttribute("context");
            MethodAndClass resultat = Utilitaire.urlExist(myMap, url);

            Class classe = Class.forName(resultat.getClasse());
            Object theObjet = classe.newInstance();
            Method[] methode = theObjet.getClass().getDeclaredMethods();
            ModelView theMethod = new ModelView();
            
            for (int i = 0; i < methode.length; i++) {
                String fonct = methode[i].getName();
                if(fonct.equals(resultat.getMethod())){
                   theMethod = (ModelView) methode[i].invoke(theObjet, null); 
                }
            }
            request.setAttribute(theMethod.getUrl(), theMethod);
            RequestDispatcher dispat = request.getRequestDispatcher("/"+theMethod.getUrl());
            dispat.forward(request, response);
        } catch (Exception e) {
           
        } finally {
            
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
